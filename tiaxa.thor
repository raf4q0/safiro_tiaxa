require 'rubygems'
require 'active_record'
require 'pry'
ActiveRecord::Base.establish_connection(
  :adapter  => 'mysql2',
  :host     => 'localhost',
  :username => 'root',
  :password => 'root',
  :database => 'safiro_tiaxa_custom',
  :port => '3306'
  )
class Sim < ActiveRecord::Base
  self.table_name = "sims"
end

class Request < ActiveRecord::Base
  self.table_name = "requests"
end

class Sympathizer < ActiveRecord::Base
  self.table_name = "structure_nodes"
end

class Tiaxa < Thor

  desc "actions", "Tiaxa commands: preactive | active | inactive | status | custom_message"
  def methods(command_action)
    if command_action == "preactive"
      method = "preactivateMobile"
      message = "Envia ALTA para activar el telefono"
      status = "inactive"
      phones(method, message, status)
    elsif command_action == "active"
      method = "activateMobile"
      message = "El telefono ha sido activado"
      status = "preactive"
      phones(method, message, status)
    elsif command_action == "inactive"
      method = "cancelMobile"
      message = "El telefono ha sido desactivado"
      status = "active"
      phones(method,message,status)
    elsif command_action == "status"
      method = "queryMobile"
      sim = Sim.all
      sims.each do |h|
        if s.sims_status != "active"
          subscriber = h.phone_number.to_i
          sims_status(method, subscriber)
        else
          puts "N°: #{s.phone_number} ya está activado"
        end
      end
    elsif command_action == "custom_message"
      sympathizer = Sympathizer.all
      sympathizers.each do |s|
        cve = []
        cve = s.cve.scan(/../)
        subscriber = s.phone_number
        entity = cve[6]
        sex = cve[7]
        year_of_birth = "19" + cve[3]
        age = Time.now.year - year_of_birth.to_i
        custom_message(subscriber, entity, sex, age)
    else
      puts "tiaxa: comando no encontrado"
    end
  end
  #--------------------------- Handsets -----------------------------#
  private
  def phones(method, message, status)
    sim = Sim.where(phone_status: status, entity_id: # Aqui el entity_id)
    sims.each do |s|
      subscriber = s.phone_number.to_i
      sms_bulk(method,subscriber,message)
    end
  end
  #--------------------------- SMS Bulk -----------------------------#
  private
  def sms_bulk(method, subscriber, message)
    require 'net/http'
    require 'net/https'
    require 'uri'

    puts method
    puts subscriber
    puts message

    url = URI.parse("https://smsmkt.telcel.com/servicesSMSBulk/subscriber.jws")
    req = Net::HTTP::Post.new(url.path)
    req.basic_auth 'GK_5500008331', '742G8xN4'
    req.set_form_data({"method"=>method,"subscriber"=>subscriber, "message" => message})

    sock = Net::HTTP.new(url.host, url.port)
    sock.use_ssl = true
    res = sock.start {|http| http.request(req) }
    c_code = res.body.match /(\d+)/
    request = method
    logs(subscriber, request, c_code)
  end
  #------------------ Sims status Request ----------------------------#
  private
  def sims_status(method, subscriber)
    require 'net/http'
    require 'net/https'
    require 'uri'
    puts method
    puts subscriber

    url = URI.parse("https://smsmkt.telcel.com/servicesSMSBulk/subscriber.jws")
    req = Net::HTTP::Post.new(url.path)
    req.basic_auth 'GK_5500008331', '742G8xN4'
    req.set_form_data({"method"=>method,"subscriber"=>subscriber})

    sock = Net::HTTP.new(url.host, url.port)
    sock.use_ssl = true
    res = sock.start {|http| http.request(req) }
    c_code = res.body.match /(\d+)/
    c_subStatus = res.body.match /\=(\D+)/
    request = method
    sim = Sim.find_by_phone_number subscriber
    if c_code[1] == '0'
      if sim.phone_status == 'preactive' && c_subStatus[1] == 'ACTIVE'
        puts "Usuario dado de alta"
        sim.phone_status = 'active'
        sim.save
        r = Request.new
        r.phone_number = subscriber
        r.request = request
        r.response = "Usuario dado de alta"
        r.created_at = Time.now
        r.save
      elsif handsets.phone_status == 'preactive' && c_subStatus[1] == 'PREACTIVE'
        puts "En espera de la confirmación del usuario para darlo de alta"
        r = Request.new
        r.phone_number = subscriber
        r.request = request
        r.response = "En espera de la confirmación del usuario"
        r.created_at = Time.now
        r.save
      elsif handsets.phone_status == 'active' && c_subStatus[1] == 'INACTIVE'
        puts "Usuario dado de baja"
        sim.phone_status = 'inactive'
        sim.save
        r = Request.new
        r.phone_number = subscriber
        r.request = request
        r.response = "Usuario dado de baja"
        r.created_at = Time.now
        r.save
      end
    else
      puts "Código error: #{c_code[1]}"
      if c_code[1] != "0"
        logs(subscriber, request, c_code)
      end
    end
  end
  #--------------------------- LOGS RESPONSES -----------------------------#
  private
  def logs(subscriber, request, c_code)
    r = Request.new
    case c_code[1]
    when "0"
      sim = Sim.find_by_phone_number subscriber
      case sim.phone_status
      when 'inactive'
        sim.phone_status = 'preactive'
      when 'preactive'
        sim.phone_status = 'active'
      when 'active'
        sim.phone_status = 'inactive'
      when 'custom'
        sim.phone_status = 'custom'
      end
      sim.save
      response = "Operación exitosa"
      puts response
    when "100"
      response = "Error interno, servicio no disponible"
      puts response
    when "101"
      response = "Suscriptor no pudo ser dado de Alta"
      puts response
    when "102"
      response = "Suscriptor no pudo ser dado de Baja"
      puts response
    when "103"
      response = "Suscriptor no pudo ser dado de Baja"
      puts response
    when "106"
      response = "Formato mensaje de texto Invalido"
      puts response
    when "107"
      response = "El móvil no está provisionado"
      puts response
    when "108"
      response = "Formato MDN inválido"
      puts response
    when "109"
      response = "No se definió una acción válida"
      puts response
    when "500"
      response = "El número no se pudo comprobar cómo usuario Telcel"
      puts response
    end
    r.phone_number = subscriber
    r.request = request
    r.response = response
    r.created_at = Time.now
    r.save
  end
  private
  def custom_message(method, subscriber, entity, sex, age)

    case entity

    when "01" # type_message in Aguascalientes

      sympathizer = Sympathizer.all

      if age > 40
        message = "\"Tecnología y capacitación de la policía, para la seguridad de tu familia\".\n
                  Para que lo bueno pase en Aguascalientes. Este 5 de Junio, Vota PRI.\n
                  https://www.facebook.com/lorenamartinezr/videos/1156495374371792/"
        sympathizer.entity_id = entity
        sympathizer.phone_number = subscriber
        sympathizer.message = message
        sympathizer.save

      if age > 40 and sex == "M"
        message = "\"Más apoyos para mujeres jefas de familia\".\n
                  Juntos Hacemos Más. Este 5 de Junio Vota PRI.\n
                  http://pri.org.mx/JuntosHacemosMas/Candidatos/Aguascalientes.aspx"
        sympathizer.entity_id = entity
        sympathizer.phone_number = subscriber
        sympathizer.message = message
        sympathizer.save

      if age > 40
        message = "\"Que tus hijos tengan acceso a ciencia y tecnología para una mejor educación\".\n
                  Para que lo bueno pase. Este 5 de junio Vota PRI.\n
                  https://www.facebook.com/lorenamartinezr/videos/1108997462454917/"
        sympathizer.entity_id = entity
        sympathizer.phone_number = subscriber
        sympathizer.message = message
        sympathizer.save

      if age > 40 and sex == "H"
        message = "\"Capacitación a trabajadores mayores de 40 años para obtener empleos mejor pagados\".\n
                  Juntos Hacemos Más. Este 5 de Junio Vota PRI.\n
                  https://www.facebook.com/lorenamartinezr/videos"
        sympathizer.entity_id = entity
        sympathizer.phone_number = subscriber
        sympathizer.message = message
        sympathizer.save

      if age > 17 and age < 25
        message = "\"Más becas y vinculación de Universidades con Empresas, para empleos mejor pagados\".\n
                  Juntos Hacemos Más. Este 5 de Junio Vota PRI.\n
                  https://www.facebook.com/lorenamartinezr/videos/1142660202421976/"
        sympathizer.entity_id = entity
        sympathizer.phone_number = subscriber
        sympathizer.message = message
        sympathizer.save

      if age > 24 and age 39
        message = "\"Microcréditos sin intereses para iniciar o hacer crecer tu negocio\".\n
                  Juntos Hacemos Más. Este 5 de Junio Vota PRI.\n
                  https://www.facebook.com/lorenamartinezr/videos/1156499717704691/"
        sympathizer.entity_id = entity
        sympathizer.phone_number = subscriber
        sympathizer.message = message
        sympathizer.save

    when "08" # type_message Chihuahua

      if age > 17
        message = "\"Microcréditos sin intereses para iniciar o hacer crecer tu negocio\".\n
                  Juntos Hacemos Más. Este 5 de Junio Vota PRI.\n
                  https://www.facebook.com/lorenamartinezr/videos/1156499717704691/"
        sympathizer.entity_id = entity
        sympathizer.phone_number = subscriber
        sympathizer.message = message
        sympathizer.save

      if age > 40 and sex == "H"
        message = "\"Inversiones en todo el estado para crear empleos mejor pagados\".\n
                  Juntos Hacemos Más. Este 5 de Junio Vota PRI.\n
                  http://pri.org.mx/JuntosHacemosMas/Candidatos/Chihuahua.aspx"
        sympathizer.entity_id = entity
        sympathizer.phone_number = subscriber
        sympathizer.message = message
        sympathizer.save

      if age > 17 and age < 40
        message = "\"Programa Emprendedor Total, crédito a la palabra para iniciar tu negocio\".\n
                  Juntos Hacemos Más. Este 5 de Junio Vota PRI.\n
                  https://www.facebook.com/enrique.serrano.351/posts/831844490293253"
        sympathizer.entity_id = entity
        sympathizer.phone_number = subscriber
        sympathizer.message = message
        sympathizer.save

      if age > 40
        message = "\"Espacios públicos para disfrutar con tu familia\".\n
                  Para hacer que lo bueno pase. Este 5 de junio, Vota PRI.\n
                  https://www.facebook.com/enrique.serrano.351/posts/831844490293253"
        sympathizer.entity_id = entity
        sympathizer.phone_number = subscriber
        sympathizer.message = message
        sympathizer.save

      if age > 40 and sex == "M"
        message = "\"Más apoyos a mujeres jefas de familia\".\n
                  Juntos hacemos más. Este 5 de junio, Vota PRI.\n
                  https://www.facebook.com/enrique.serrano.351/videos/vb.497992153678490/823916521086050/?type=2&amp;theater"
        sympathizer.entity_id = entity
        sympathizer.phone_number = subscriber
        sympathizer.message = message
        sympathizer.save

      if age > 17 and age < 25
        message = "\"Becas y titulación gratis para los universitarios. Juntos Hacemos más. Este 5 de Junio, Vota PRI\".\n
                  Para hacer que lo bueno pase, este 5 de junio Vota PRI.\n
                  https://www.facebook.com/enrique.serrano.351/posts/831844490293253"
        sympathizer.entity_id = entity
        sympathizer.phone_number = subscriber
        sympathizer.message = message
        sympathizer.save

  when "09" # type_message Ciudad de México

    if age > 17
      message = "\"Becas y titulación gratis para los universitarios. Juntos Hacemos más. Este 5 de Junio, Vota PRI\".\n
                Para hacer que lo bueno pase, este 5 de junio Vota PRI.\n
                https://www.facebook.com/enrique.serrano.351/posts/831844490293253"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 17
      message = "\"Derecho a la movilidad: Transporte público eficiente, suficiente y seguro\".\n
                Juntos hacemos más. Este 5 de Junio, Vota PRI.\n
                http://youtube.be/5Rs28JMuM"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 17 and sex == "M"
      message = "\"Por tu derecho a caminar segura por las calles\".\n
                Juntos hacemos más. Este 5 de Junio, Vota PRI.\n
                http://youtube.be/5Rs28JMuM"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

  when "10" # type_message Durango

    if age > 17
      message = "\"Ideas Nuevas para el Futuro de los Duranguenses\".\n
                Para que tu ganes más, este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/evillegasv/videos/1219058694779672/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if (age > 40 and age < 60) and sex == "H"
      message = "\"Con el corredor industrial automotriz, crearemos 70 mil empleos bien pagados\".\n
                Para que tu ganes más, este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/evillegasv/videos/1217000544985487/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 35
      message = "\"Para que dejes de pagar tenencia y reemplacamiento vehicular\".\n
                Este 5 de junio, juntos hacemos más, vota PRI.\n
                https://www.facebook.com/evillegasv/videos/1193347590684116/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 65
      message = "\"Incentivos a las empresas que contraten a adultos mayores\".\n
                Para que tu ganes más, este 5 de junio, Vota PRI.\n
                https://www.facebook.com/evillegasv/videos/1215675268451348/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

  when "13" # type_message Durango

    if age > 35
      message = "\"Más inversión, para crear empleos mejor pagados\".\n
                Por ti y tu familia. Este 5 de Junio, Vota PRI.\n
                https://www.youtube.com/watch?v=fO6s-jRHkbw"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if (age > 17 and age < 25) || (age > 35 and sex == "M")
      message = "\"Internet gratis en todas las escuelas públicas para una mejor educación\".\n
                Para Hacer Que Lo Bueno Pase, Este 5 de Junio, Vota PRI.\n
                https://twitter.com/omarfayad/status/735194109560815616"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 17 and age < 25
      message = "\"Subsidio a los primeros 4 sueldos de los egresados universitarios para que formen experiencia profesional\".\n
                Por ti y tu familia, este 5 de junio, Vota PRI.\n
                https://www.facebook.com/omarfayadmeneses/videos/1730683947143926/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 39 and age < 56
      message = "\"Microcréditos a mujeres para que puedan empezar un negocio\".\n
                Juntos Hacemos Más, este 5 de junio, vota PRI.\n
                https://www.facebook.com/omarfayadmeneses/?fref=ts"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 40
      message = "\"Brigadas de salud en todo el estado, para cuidar a tu familia\".\n
                Por ti y tu familia, este 5 de junio, vota PRI.\n
                https://www.facebook.com/omarfayadmeneses/videos/1737482826464038/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

  when "20" # type_message Oaxaca

    if age > 40
      message = "\"Orden y transparencia para que Oaxaca crezca\".\n
                Juntos hacemos más. Este 5 de Junio, Vota PRI.\n
                https://www.youtube.com/watch?v=WHoIXF8l9jE"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 17 and age < 40
      message = "\"Instituto del Emprendedor para que inicies tu negocio\".\n
                Juntos hacemos más. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/AlejandroMuratHinojosa/videos/1071455509615993/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 17
      message = "\"Un Oaxaca incluyente, con crecimiento para todos\".\n
                Juntos, sí se puede. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/AlejandroMuratHinojosa/?fref=ts"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 17 and age < 26
      message = "\"Becas para estudiantes e Internet para todos\".\n
                Juntos, sí se puede. Este 5 de Junio, Vota PRI.\n
              https://www.youtube.com/watch?v=gRoTCy9SsRo"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 17
      message = "\"Maestros mejor preparados y mejor pagados\".\n
                Juntos hacemos más. Este 5 de Junio, Vota PRI.\n
              http://pri.org.mx/JuntosHacemosMas/Candidatos/Oaxaca.aspx"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 17 and sex == "M"
      message = "\"Salud y Seguridad Social para todas y todos los Oaxaqueños\".\n
                Para hacer que lo bueno pase, este 5 de junio, vota PRI.\n
                https://www.facebook.com/AlejandroMuratHinojosa/photos/a.280427985385420.64885.259628454132040/1083581445070066/?type=3&amp;theater"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

  when "21"  # type_message Puebla

    if age > 17
      message = "\"Obras en todo el estado, que atiendan tus necesidades. No más obras caras e inútiles\".\n
                Por tu familia. Este 5 de Junio, Vota PRI.\n
                http://pri.org.mx/JuntosHacemosMas/Candidatos/Puebla.aspx"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 24 and age < 40
      message = "\"Créditos y capacitación para idear, abrir y hacer crecer tu propio negocio\".\n
                Para que lo bueno pase, Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/SoyBlancaAlcala/videos/972081012888368/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 24 and age < 51
      message = "\"Jornadas reducidas, horarios maternales y estancias infantiles para madres trabajadoras\".\n
                Para que lo bueno pase, este 5 de junio, Vota PRI.\n
                http://d3n8a8pro7vhmx.cloudfront.net/themes/56e9a77c6a21db9c57000001/attachments/original/1464101633/MUJER_EMPODERAMIENTO.gif"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 24 and age < 61
      message = "\"Apoyos para que las mujeres puedan cumplir sus sueños e iniciar su propio negocio\".\n
                Para que lo bueno pase, este 5 de junio, Vota PRI.\n
                https://www.facebook.com/SoyBlancaAlcala/?fref=ts"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 17
      message = "\"Acciones que garanticen la justicia, para la seguridad y tranquilidad de tu familia\".\n
                Juntos Hacemos Más. Este 5 de junio, Vota PRI.\n
                https://www.facebook.com/SoyBlancaAlcala/photos/a.135753969854414.27451.134373033325841/971429502953519/?type=3&amp;theater"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

  when "23" # type_message Quintana Roo

    if (age > 29 and age < 46) and sex == "M"
      message = "\"Clases de inglés gratis en las escuelas\".\n
                Por tu familia. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/MauricioGongoraE/videos/1097050383685045/Hombres y mujeres de 25 a 50 años"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 24 and age < 51
      message = "\"Programas de enseñanza de inglés para empleos mejor pagados\".\n
                Para que lo bueno pase. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/MauricioGongoraE/videos/1092210867502330/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 30 and age < 46
      message = "\"Estancias infantiles con horario extendido para madres trabajadoras\".\n
                Por tu familia. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/MauricioGongoraE/videos/1079504682106282/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 30
      message = "\"Sistema de videovigilancia y alerta vecinal, par la seguridad de tu familia\".\n
                Por tu familia. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/MauricioGongoraE/?fref=ts"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 17
      message = "\"Que los recursos naturales de Quintana Roo se cuiden y sean para todos\".\n
                Para que las cosas buenas pasen, este 5 de junio, vota PRI.\n
                https://www.facebook.com/MauricioGongoraE/?fref=ts"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 65
      message = "\"Fuentes de empleo y acceso a servicios de salud para adultos mayores\".\n
                Juntos Hacemos Más, este 5 de Junio, Vota PRI.\n
                http://pri.org.mx/JuntosHacemosMas/Candidatos/Quintana_Roo.aspx"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

  when "25" # type_message Sinaloa

    if age > 17 and age < 26
      message = "\"Beca salario para estudiantes universitarios\".\n
                Para que lo bueno pase. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/quirinoordazcoppel/videos/624298954386610/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 65
      message = "\"Incentivos a las empresas que contraten adultos mayores\".\n
                Juntos hacemos más. Este 5 de Junio, Vota PRI.\n
                http://pri.org.mx/JuntosHacemosMas/Candidatos/Sinaloa.aspx"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 17 and age < 36
      message = "\"Internet gratis y rápido\".\n
                Juntos hacemos más. Este 5 de Junio, Vota PRI.\n
                http://pri.org.mx/JuntosHacemosMas/Candidatos/Sinaloa.aspx"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 35
      message = "\"Sello de Calidad ¡Puro Sinaloa! para que nuestros productos se paguen a buen precio dentro y fuera del país.\".\n
                Juntos hacemos más. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/quirinoordazcoppel/videos/627415297408309/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 30 and sex == "M"
      message = "\"Guarderías con horarios extendidos para madres trabajadoras.\".\n
                Juntos hacemos más. Este 5 de Junio, Vota PRI.\n
                https://web.facebook.com/quirinoordazcoppel/videos/625333230949849/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 17 and sex == "M"
      message = "\"Creación de la Unidad Especializada contra la Violencia hacia las Mujeres.\".\n
                Para hacer que lo bueno pase, este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/quirinoordazcoppel/videos/615987528551086/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if (age > 27 and age < 41) and sex == "H"
      message = "\"Más apoyos para que puedas iniciar y hacer crecer tu negocio.\".\n
                Para Hacer Que Lo Bueno Pase, este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/quirinoordazcoppel/videos/612184888931350/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

  when "28" # type_message Tamaulipas

    if (age > 27 and age < 41) and sex == "M"
      message = "\"El programa más amplio de guarderías y estancias infantiles para mujeres trabajadoras\".\n
                Por tu familia. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/BaltazarMHO/posts/10154313405978083"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if (age > 27 and age < 56) and sex == "M"
      message = "\"Programa de créditos para que hacer crecer los negocios de las mujeres\".\n
                Por el bien de Tamaulipas. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/BaltazarMHO/?fref=ts"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if (age > 17 and age < 26) || (age > 39 and age < 51) and sex == "M"
      message = "\"Prepa para todos e incentivos a las empresas que contraten recién egresados\".\n
                Por la Paz de Tamaulipas. Este 5 de Junio, Vota PRI.\n
                https://www.youtube.com/watch?v=1fR2-7Jx2KQ"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 17 and sex == "M"
      message = "\"Paz y seguridad para las familias de Tamaulipas. Unidad Antisecuestro y Sistema de videovigilancia en calles y caminos\".\n
                Por tu familia. Este 5 de Junio, Vota PRI.\n
                https://www.youtube.com/watch?v=kDu6sVJLwvs"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 65
      message = "\"Espacios de esparcimiento y servicios de salud para adultos mayores\".\n
                Por la Paz de Tamaulipas, este 5 de Junio, Vota PRI.\n
                http://pri.org.mx/JuntosHacemosMas/Candidatos/Tamaulipas.aspx"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 65
      message = "\"Incentivos y créditos para adultos mayores que se autoemplean\".\n
                Por el bien de tamaulipas, este 5 de Junio, Vota PRI.\n
                http://pri.org.mx/JuntosHacemosMas/Candidatos/Tamaulipas.aspx"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 29 and age < 65
      message = "\"Incentivos a empresas que inviertan en Tamaulipas, para crear empleos mejor pagados\".\n
                Para hacer que lo bueno pase, este 5 de Junio, Vota PRI\n
                https://www.youtube.com/watch?v=1fR2-7Jx2KQ"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

  when "29" # type_message Tlaxcala

    if age > 17
      message = "\"Nuevas empresas e industrias para crear el mayor número de empleos, bien pagados, en la historia de Tlaxcala\".\n
                Juntos hacemos más. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/marco.a.mena/videos/1255966741110339/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if (age > 17 and age < 25) || (age > 39 and age < 51) and sex == "M"
      message = "\"Un gran programa de becas para que los jóvenes estudien\".\n
                Por tu familia. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/marco.a.mena/videos/1253101641396849/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if (age > 34 and age < 51) and sex == "M"
      message = "\"Apoyo a jefas de familia para iniciar un negocio\".\n
                Que lo bueno pase. Este 5 de Junio, Vota PRI.\n
                http://pri.org.mx/JuntosHacemosMas/Candidatos/Tlaxcala.aspx"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 35
      message = "\"Policía capacitada y equipada para tu seguridad y la de tu familia\".\n
                Por tu familia. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/marco.a.mena/videos/1255283194512027/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 17 and age < 36
      message = "\"Internet para todos, para el desarrollo de Tlaxcala\".\n
                Juntos Hacemos Más. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/marco.a.mena/videos/1255147267858953/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if ((age > 17 and age < 46) and sex == "H") || ((age > 17 and age < 35) and sex == "M")
      message = "\"Créditos para iniciar y hacer crecer tu negocio\".\n
                Juntos Hacemos Más. Este 5 de Junio, Vota PRI.\n
                http://pri.org.mx/JuntosHacemosMas/Candidatos/Tlaxcala.aspx"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

  when "30" # type_message Veracruz

    if age > 17 and age < 25
      message = "\"Más becas para estudiantes en todo el estado\".\n
                Por Un Nuevo Veracruz. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/HectorYunes/videos/10154146024239808/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 23 and age < 31
      message = "\"Programas entre universidades y empresas para que los jóvenes tengan empleo\".\n
                Un Nuevo Veracruz. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/HectorYunes/videos/10154140381469808/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 17
      message = "\"Funcionarios Honestos y Cero Tolerancia a la Corrupción\".\n
                Por Un Nuevo Veracruz. Este 5 de Junio, Vota PRI.\n
                https://twitter.com/HectorYunes/status/734560113306927104"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 17
      message = "\"Tecnología y policía especializada para la seguridad de tu familia\".\n
                Un nuevo Veracruz. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/HectorYunes/videos/10154151405529808/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 24 and age < 56
      message = "\"91 mil empleos mejor pagados, cada año\".\n
                Un Nuevo Veracruz. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/HectorYunes/videos/10154182904214808/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if (age > 34 and age < 56) and sex == "M"
      message = "\"Créditos para mujeres, seguros para jefas de familia y guarderías con horario extendido\".\n
                Por tu familia. Este 5 de Junio, Vota PRI.\n
                https://twitter.com/HectorYunes/status/727245559497560064"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 65
      message = "\"Espacios laborales, pago de pensiones a tiempo y una red de atención médica especializada para adultos mayores\".\n
                Un Nuevo Veracruz. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/HectorYunes/videos/10154113021904808/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

  when "32" # type_message Zacatecas

    if age > 17 and age < 40
      message = "\"Acceso a internet y herramientas digitales para crear empleos mejor pagados y más desarrollo\".\n
                Por tu familia. Este 5 de Junio, Vota PRI.\n
                https://www.youtube.com/watch?v=2fi86JyPKd0"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 34 and age < 65
      message = "\"Policía estatal única bien coordinada, para la seguridad de tu familia\".\n
                Por tu familia. Este 5 de Junio, Vota PRI.\n
                http://pri.org.mx/JuntosHacemosMas/Candidatos/Zacatecas.aspx"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if (age > 39 and age 65) and sex == "M"
      message = "\"Apoyo a mujeres para iniciar su negocio\".\n
                Por tu familia. Este 5 de Junio, Vota PRI.\n
                https://media.giphy.com/media/3oEjI69teNEOoSZvLG/giphy.gif"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if (age > 17 and age < 25) and sex == "H"
      message = "\"Fortalecer la industria y la innovación para crear empleos mejor pagados\".\n
                Una garantía para tu familia. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/TelloAlejandro/videos/1260462697312113/"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if (age > 17 and age < 25) and sex == "M"
      message = "\"Becas a madres jóvenes para que no dejen sus estudios\".\n
                Una garantía para tu futuro. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/TelloAlejandro/photos/a.399695933388798.97433.399689356722789/1259528994072150/?type=3&amp;theater"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save

    if age > 17
      message = "\"Becas a madres jóvenes para que no dejen sus estudios\".\n
                Una garantía para tu futuro. Este 5 de Junio, Vota PRI.\n
                https://www.facebook.com/TelloAlejandro/photos/a.399695933388798.97433.399689356722789/1259528994072150/?type=3&amp;theater"
      sympathizer.entity_id = entity
      sympathizer.phone_number = subscriber
      sympathizer.message = message
      sympathizer.save







  end # case entities
end
